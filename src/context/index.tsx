import React from 'react';
import { Category } from '../interfaces/category.interface';
import { Video } from '../interfaces/video.interface';

interface IContext {
  videos: Video[];
  categories: Category[];
  setVideos: (data: any) => void;
}

const Context = React.createContext<IContext>({
  videos: [],
  categories: [],
  setVideos: () => {},
});

export default Context;
