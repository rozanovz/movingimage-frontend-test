import React from 'react';
import { withRouter, Switch, Route } from 'react-router-dom';
import Home from './components/Home';
import VideoForm from './components/VideoForm';

const Routes = () => {
  return (
    <Switch>
      <Route path="/video/:id?">
        <VideoForm />
      </Route>
      <Route path="/">
        <Home />
      </Route>
    </Switch>
  )
}

export default withRouter(Routes);
