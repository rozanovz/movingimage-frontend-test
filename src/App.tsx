import React, { useEffect, useState } from 'react';
import { Container } from '@material-ui/core';
import { BrowserRouter } from 'react-router-dom';
import Routes from './Routes';
import { getVideos } from './services/videos';
import { Video } from './interfaces/video.interface';
import Context from './context'
import NavBar from './components/NavBar';
import { Category } from './interfaces/category.interface';

const App: React.FC = () => {
  const [videos, setVideos] = useState<Video[]>([]);
  const [categories, setCategories] = useState<Category[]>([]);
  const _getVideos = () => getVideos().then(({videos, categories}) => {
    setVideos(videos);
    setCategories(categories);
  });

  useEffect(() => {
    _getVideos()
  }, []);

  return (
    <>
      <BrowserRouter basename='/'>
        <NavBar />

        <Container>
          <Context.Provider value={{
            videos,
            categories,
            setVideos,
          }}>
            <Routes />
          </Context.Provider>
        </Container>
      </BrowserRouter>
    </>
  );
};

export default App;
