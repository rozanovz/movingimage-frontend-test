export interface Video {
  id?: number;
  name: string;
  author: string;
  categories: string[];
  // highestQualityFormat: string;
  // releaseDate: string;
}
