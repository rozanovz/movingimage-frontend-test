import { getCategories } from './categories';
import { getAuthors } from './authors';
import { Video } from '../interfaces/video.interface';
import { Author, AuthorVideo } from '../interfaces/author.interface';
import { Category } from '../interfaces/category.interface';

export const getVideos = (searchValue = ''): Promise<{videos: Video[], categories: Category[]}> => {
  return Promise.all([getCategories(), getAuthors()]).then(([categories, authors]) => {
    const categoriesGroupped: Record<any, any> = categories.reduce(
      (a, b) => ({
        ...a,
        [b.id]: b.name,
      }),
      {}
    );
    return {
      videos: authors
      .map((author: Author) =>
        author.videos.map((video: AuthorVideo) => ({
          ...video,
          author: author.name,
          categories: video.catIds.map((cat) => categoriesGroupped[cat]),
        }))
      )
      .reduce((a, b) => [...a, ...b], [])
      .filter(video => video.name.toLowerCase().indexOf(searchValue.toLowerCase()) !== -1),
      categories,
    };
  });
};
