import React from 'react';
import { AppBar, Button, Grid, Toolbar, Typography } from '@material-ui/core';
import { useHistory } from 'react-router';

const NavBar = () => {
  const history = useHistory();

  return (
    <AppBar position="static">
      <Toolbar>
        <Grid justify="space-between" container>
          <Grid item>
            <Typography variant="h6">Video Manager</Typography>
          </Grid>
          <Grid item>
            <Button
              variant="contained"
              color="default"
              size="small"
              onClick={() => history.push('/video')}
            >
              Add Video
            </Button>
          </Grid>
        </Grid>
      </Toolbar>
    </AppBar>
  );
};

export default NavBar;
