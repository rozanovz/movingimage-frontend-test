import { Video } from '../../interfaces/video.interface';

export interface VideosTableProps {
  videos: Video[];
  handleEdit: (id: number) => void;
  handleDelete: (id: number) => void;
  handleSort: (orderBy: keyof Video, order: string) => void;
}
