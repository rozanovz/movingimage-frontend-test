import React, { useState } from 'react';
import { 
  Paper,
  Popover,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Button,
  Box,
  Typography,
  TableSortLabel
} from '@material-ui/core';
import { VideosTableProps } from './videos-table.interface';
import { Video } from '../../interfaces/video.interface';

type Order = 'asc' | 'desc';

export const VideosTable: React.FC<VideosTableProps> = ({ 
  videos,
  handleEdit,
  handleDelete,
  handleSort,
}) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const [videoToDelete, setVideoToDelete] = useState({});
  const [order, setOrder] = React.useState<Order>('asc');
  const [orderBy, setOrderBy] = React.useState<keyof Video>('name');

  let id: string = '';

  const handlePopoverClick = (event: any, video: Video) => {
    id = `delete-popover-${video.id}`
    setAnchorEl(event.currentTarget);
    setVideoToDelete(video)
  };

  const handlePopoverClose = () => {
    id = '';
    setAnchorEl(null);
    setVideoToDelete({})
  };

  const open = Boolean(anchorEl);
  const handleRequestSort = (property: keyof Video) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
    handleSort(orderBy, order);
  };
  
  return (
    <>
      <TableContainer component={Paper} style={{ marginTop: '40px' }}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell sortDirection={orderBy === 'name' ? order : false}>
                <TableSortLabel
                  active={orderBy === 'name'}
                  direction={orderBy === 'name' ? order : 'asc'}
                  onClick={() => {handleRequestSort('name')}}
                >
                  Video Name
                </TableSortLabel>
              </TableCell>
              <TableCell sortDirection={orderBy === 'author' ? order : false}>
                <TableSortLabel
                  active={orderBy === 'author'}
                  direction={orderBy === 'author' ? order : 'asc'}
                  onClick={() => {handleRequestSort('author')}}
                >
                  Video Author
                </TableSortLabel>
              </TableCell>
              <TableCell>Categories</TableCell>
              {/* <TableCell>Highest Quality Format</TableCell> */}
              {/* <TableCell>Release Date</TableCell> */}
              <TableCell>Options</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {videos.map((video) => (
              <TableRow key={video.id}>
                <TableCell component="th" scope="row">
                  {video.name}
                </TableCell>
                <TableCell>{video.author}</TableCell>
                <TableCell>{video.categories.join(', ')}</TableCell>
                {/* <TableCell>{video.highestQualityFormat}</TableCell> */}
                {/* <TableCell>{video.releaseDate}</TableCell> */}
                <TableCell>
                  <Button
                    variant="contained" 
                    size="small" 
                    color="primary"
                    onClick={() => {handleEdit(video.id as number)}}
                  >
                    Edit
                  </Button>

                  <Box ml={2} display="inline">
                    <Button
                      variant="contained" 
                      size="small" 
                      color="secondary"
                      onClick={(event) => handlePopoverClick(event, video)}
                    >
                      Delete
                    </Button>
                  </Box>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>

      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handlePopoverClose}
        anchorOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'center',
        }}
      >
        <Box m={1}>
          <div>
            <Typography>Are you sure you want to delete {(videoToDelete as Video).name}?</Typography>
          </div>

          <div>
            <Button
              variant="contained"
              size="small"
              color="secondary"
              onClick={() => { 
                handleDelete((videoToDelete as Video).id as number);
                handlePopoverClose();
              }}
            >
              Confirm
            </Button>

            <Box ml={2} display="inline">
              <Button
                variant="contained"
                size="small"
                color="default"
                onClick={handlePopoverClose}
              >
                Cancel
              </Button>
            </Box>
          </div>
        </Box>
      </Popover>
    </>
  );
};
