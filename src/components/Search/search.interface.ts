export interface SearchProps {
  search: (value: string) => void;
}
