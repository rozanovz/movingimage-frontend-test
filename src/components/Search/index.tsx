import React, {useState} from 'react'
import { TextField, Button, Box } from '@material-ui/core';
import { SearchProps } from './search.interface';
import { useInputStyles } from './styles';

const Search = ({ search }: SearchProps) => {
  const [searchValue, setSearchValue] = useState('');
  const handleFieldChange = (e: any) => setSearchValue(e.target.value);
  const handleSearch = () => search(searchValue);
  const classes = useInputStyles();

  return (
    <Box mt={2}>
      <TextField
        variant="outlined"
        value={searchValue}
        onChange={handleFieldChange}
        className={classes.root}
      />
      <Button variant="contained" color="primary" onClick={handleSearch}>
        Search
      </Button>
    </Box>
  )
}

export default Search;