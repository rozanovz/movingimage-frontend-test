import { makeStyles } from '@material-ui/core/styles';

export const useInputStyles = makeStyles({
  root: {
    width: '300px',
    '& input' : {
      padding: '9px 5px',
    },
  },
});
