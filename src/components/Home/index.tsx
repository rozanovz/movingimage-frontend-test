import React, { useContext, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { VideosTable } from '../VideoTable';
import Search from '../Search';
import Context from '../../context';
import { Video } from '../../interfaces/video.interface';

const Home: React.FC = () => {
  const {videos, setVideos} = useContext(Context);
  const [searchValue, setSearchValue] = useState('');
  const history = useHistory();

  const search = (searchValue: string) => {
    setSearchValue(searchValue)
  }

  const handleEdit = (id: number) => {
    history.push(`/video/${id}`);
  }

  const handleDelete = (id: number) => {
    setVideos(videos.filter((video: Video) => video.id !== id));
  }

  const handleSort = (field: keyof Video, direction: string) => {
    setVideos(videos.sort((a: Record<string, any>, b: Record<string, any>) => {
      if (a[field] < b[field]){
        return direction === 'asc' ? 1 : -1;
      }
      if (a[field] > b[field]){
        return direction === 'asc' ? -1 : 1;
      }
      return 0;
    }));
  }

  return (
    <>
      <Search search={search}/>
      <VideosTable 
        videos={videos.filter((video: Video) => video.name.toLowerCase().indexOf(searchValue.toLowerCase()) !== -1)}
        handleEdit={handleEdit}
        handleDelete={handleDelete}
        handleSort={handleSort}
      />
    </>
  );
};

export default Home;
