import { makeStyles } from '@material-ui/core/styles';

export const useInputStyles = makeStyles({
  root: {
    width: '100%',
    '& input' : {
      padding: '10px 5px',
    },
    '& select' : {
      padding: '10px 5px',
    }
  },
});
