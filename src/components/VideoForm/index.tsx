import React, { useContext, useState, useEffect } from 'react';
import { useHistory, useParams } from "react-router-dom";
import Context from '../../context';
import { Video } from '../../interfaces/video.interface';
import { Box, Button, Grid, Select, TextField, Typography } from '@material-ui/core';
import { useInputStyles } from './styles';

const initialVideo = {
  name: '',
  author: '',
  categories: [],
};

const VideoForm: React.FC = () => {
  const fieldsClasses = useInputStyles();
  const history = useHistory();
  
  const { id } = useParams<{id: string}>();
  const { videos, setVideos, categories } = useContext(Context);
  
  const [video, setVideo] = useState<Video>(initialVideo);

  useEffect(() => {
    if(id){
      const videoById = videos.find((v: Video) => v.id === Number(id));
      if(!videoById){
        history.push('/')
      }
      setVideo({ ...video, ...videoById });
    } else {
      setVideo({ ...initialVideo });
    }
  }, [id]);
  
  const heading = id ? `Edit Video: ${video.name}` : 'Add Video';

  const validateField = (field: keyof Video) => !(video as Record<string, any>)[field].length;
  const isFormValid = () => !video.name.length || !video.author.length || !video.categories.length;

  const handleChange = (id: string, e: any) => setVideo({ ...video, [id]: e.target.value });
  const handleChangeMultiple = (e: any) => {
    const { options } = e.target;
    const value = [];
    for (let i = 0, l = options.length; i < l; i += 1) {
      if (options[i].selected) {
        value.push(options[i].value);
      }
    }
    handleChange('categories', { target: { value } });
  };

  const handleSubmit = () => {
    if (id) {
      setVideos([ ...videos.map((v:Video) => ({
        ...v, 
        ...(v.id === video.id ? { ...video } : {})
      }))])
    } else {
      const maxId = Math.max(...videos.map(({id}) => id) as number[]);
      setVideos([ ...videos, { ...video, id: maxId + 1 }]);
    }
    history.push('/');
  }

  const fields = [
    {
      id: 'name',
      label: 'Video name',
      component: (
        <TextField 
          id="name"
          variant="outlined"
          value={video.name}
          onChange={(e) => handleChange('name', e)}
          className={fieldsClasses.root}
          error={validateField('name')}
        />
      )
    },
    {
      id: 'author',
      label: 'Video author',
      component: (
        <TextField 
          id="author"
          variant="outlined"
          value={video.author}
          onChange={(e) => handleChange('author', e)}
          className={fieldsClasses.root}
          error={validateField('author')}
        />
      )
    },
    {
      id: 'categories',
      label: 'Video category',
      component: (
        <Select
          multiple
          native
          variant="outlined"
          className={fieldsClasses.root}
          value={video.categories}
          onChange={handleChangeMultiple}
          error={validateField('categories')}
        >
          {categories.map((category) => (
            <option key={category.id} value={category.name}>
              {category.name}
            </option>
          ))}
        </Select>
      )
    },
    {
      id: 'buttons',
      label: '',
      component: (
        <>
          <Button
            size="small"
            variant="contained"
            color="primary"
            onClick={handleSubmit}
            disabled={isFormValid()}
          >
            Submit
          </Button>

          <Box ml={2} display="inline">
            <Button 
              size="small" 
              variant="contained" 
              color="default" 
              onClick={() => {history.push('/')}}
            >
              Cancel
            </Button>
          </Box>
        </>
      )
    }
  ];
  
  return (
    <>
      <h1>{heading}</h1>
      <div>
        {
          fields.map(field => (
            <Grid container spacing={3} key={field.id}>
              <Grid item xs={2}>
                <Typography>{ field.label }</Typography>
              </Grid>
    
              <Grid item xs={10}>
                { field.component }
              </Grid>
            </Grid>
          ))
        }
      </div>
    </>
  );
};

export default VideoForm;
